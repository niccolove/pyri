# First section

<!--
These are comments. They are not sent to the user.
Section names are mostly to link them, they are also
not sent to the users.
Normal text is sent to the user, in the order that it
is found, one message per paragraph.
To give the user a list of choiches, put them in a quote
with a link to the section that should be triggered by
that particular reply.
Code sections will be evaluated right away.
-->

Welcome in, my friend!
I'm **really happy** to *have* you here.

The bot will start shortly.

## Beginning

Hello, and welcome to the bot.

<!--
This is the only decision in here
Anyway, FYI, we only have two variables
declared in this environment, both dicts:
"user" each user has one dictionary
"gram" one to track genera things
Currently, only JSON serializable
objects are supported :-)
-->

```
user['life'] = 100
if 'users' not in gram: gram['users'] = 0
gram['users'] += 1
print(f"Currently we have {gram['users']} users")
```

> [Welcome to you too!](#nice)

> [Hey, fuck you](#fuck-you-too)

## Nice


<!-- embed code in message to eval it -->
Nice to see you here, your life is `user['life']`!

```
print("This is the end of this code section")
```

> [This looks pretty, I gotta say!](#the-end)

## Fuck You Too

What!? Fuck you too, man

```
print("What a dick!")
```

> [You know what? Maybe I should just leave](#the-end)

## The End

Indeed. Bye!

> [It's not over](next.md#hello) <!-- You can link other files too -->
