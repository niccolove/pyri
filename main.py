from telegram.ext import Updater, MessageHandler, Filters
from telegram import Bot, ReplyKeyboardMarkup, ReplyKeyboardRemove
from dataclasses import dataclass
import os
import time
import os.path
import logging
import json

@dataclass
class DataFolder:
    path: str

    def __post_init__(self):
        self.mdfiles = [MdFile(self.path, name) for name in os.listdir(self.path)
                        if name.endswith('.md')]

    def fetch(self, filename, sectionname, defaultmd=None):
        return next((m for m in self.mdfiles if m.filename == filename),
                    defaultmd).section_named(sectionname)

    @property
    def starting_md(self): return next(m for m in self.mdfiles if m.filename == 'main.md')

@dataclass
class UserDict:
    user_id: str = ''

    def __post_init__(self):
        self.filename, self.data = f'data/{self.user_id or "global"}.json', {}
        if not os.path.isfile(self.filename): open(self.filename, 'w').write('{}')
        else: self.data = json.load(open(self.filename))

    def set_section(self, sectionname, filename):
        self.data['__section'], self.data['__filename'] = sectionname, filename

    def get_section(self, message, datafolder):
        if '__section' not in self.data: return datafolder.starting_md.starting_section
        current = datafolder.fetch(self.data['__filename'], self.data['__section'])
        if not message in current.links: return None
        return datafolder.fetch(*current.links[message], current.parent)

    def save(self): json.dump(self.data, open(self.filename, 'w'))

@dataclass
class Section:
    name: str
    messages: list[str, ...]
    links: dict[str, tuple[str, str]]
    code: str
    parent: 'MdFile'
    
    def __post_init__(self):
    	print('New section loaded:')
    	print(self.name)
    	print(self.messages)
    	print(self.links)
    	print(self.code)
    	print()

    def enter(self, bot, user, gram):
        user.set_section(self.name, self.parent.filename)
        exec(self.code, {'user': user.data, 'gram': gram.data})
        for message in filter(len, self.messages):
            time.sleep(1)
            bot.send_message(user.user_id, self.eval_message(message, user, gram),
                             'Markdown', reply_markup=self.keyboard)
        user.save(), gram.save()
        if not self.links and (next_section := self.parent.section_after(self)):
            next_section.enter(bot, user, gram)

    @property
    def keyboard(self):
        return ReplyKeyboardMarkup([[a] for a in self.links]) if self.links else None

    @staticmethod
    def eval_message(message, user, gram):
        while '`' in message:
            before, _, partial = message.partition('`')
            code, _, after = partial.partition('`')
            message = before + str(eval(code, {'gram': gram.data, 'user': user.data})) + after
        return message

@dataclass
class MdFile:
    path: str
    filename: str

    def __post_init__(self):
        md = open(os.path.join(self.path, self.filename)).read()
        while '<!--' in md: md = md[:md.index('<!--')] + md[md.index('-->')+3:]
        md, self.sections = md.strip().split('\n'), []
        while md: self.sections.append(self.parse_section(md, self))

    @property
    def starting_section(self): return self.sections[0]

    def section_named(self, name):
        return next(s for s in self.sections if s.name == name)

    def section_after(self, section):
        return next((b for a, b in zip(self.sections, self.sections[1:])
                    if a.name == section.name), None)

    @staticmethod
    def parse_section(md, parent):
        name = md.pop(0).replace('#', '').strip().replace(' ', '-').lower()
        messages, links, code = [''], {}, ''
        while md:
            if (l := md.pop(0).strip()).startswith('>'):
                l = l.replace('>', '').replace('[', '').replace(')', '')
                message, _, link = l.partition('](')
                link_file, _, link_section = link.partition('#')
                links[message.strip()] = (link_file.strip(), link_section.strip())
            elif l.startswith('```'):
                assert l == '```'
                while not (l := md.pop(0)).endswith('```'): code += l + '\n'
            elif l == '' and messages[-1] != '': messages.append('')
            elif l.startswith('#'):
                md.insert(0, l)
                break
            elif l: messages[-1] += l + '\n'
        return Section(name, messages, links, code, parent)

updater = Updater(token='424921715:AAF4kGNjtVxmPO493NS6MzMFqB_dybRL-7Q',
                  use_context=True)
dispatcher = updater.dispatcher

logging.basicConfig(format='%(asctime)s - %(name)s -'
                    '%(levelname)s - %(message)s',
                    level=logging.INFO)

gram = UserDict()
docs = DataFolder('documents/')

def reply(update, context):
    user = UserDict(update.effective_chat.id)
    section = user.get_section(update.message.text, docs)
    if not section: return context.bot.send_message(user.user_id, "COSA!?")
    section.enter(context.bot, user, gram)

echo_handler = MessageHandler(Filters.text & (~Filters.command), reply)
dispatcher.add_handler(echo_handler)
updater.start_polling()
